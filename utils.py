import json
import re
import time

# Power series values
build_power_h = {i: 0 for i in range(12)}

# Condition/transition matrix
condition_transition = [[0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0],
                        ]


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    ser = userdata['ser']
    
    # Decode and parse the payload
    payload = json.loads(msg.payload.decode())
    # print(payload)

    if 'shared' in payload:
        for key, value in payload['shared'].items():                                # Multiple power series values update 
            # Regular expression to match an integer after "build_power_h"
            match = re.search(r'(?<=build_power_h)\d+', key)
            if match:
                hour = int(match.group())
                # print(f"Hour: {hour}, Value: {value}")
                build_power_h[hour] = value
                # send_power_value(ser, hour)
                # time.sleep(0.1)     # wait a bit to avoid serial port buffer overflow
        return
    elif (len(payload) == 1):                                                         
        for key, value in payload.items():                                          # Single power series value update
            if key.startswith("build_power_h"):
                # Regular expression to match an integer after "build_power_h"
                match = re.search(r'(?<=build_power_h)\d+', key)
                if match:
                    hour = int(match.group())
                    print(f"Hour: {hour}, Value: {value}")
                    build_power_h[hour] = value
                    send_power_value(ser, hour)
                return
    elif (('method' in payload) and (payload['method'] == "setValue")):                 # Start/Stop cycle
        if payload['params']:
            userdata['flags']['start_acquisition_flag'] = True
            print("Set start_acquisition_flag")
            print("userdata['flags']['data_acquisition'] : ")
            print(userdata['flags']['data_acquisition'])
        else:
            userdata['flags']['stop_acquisition_flag'] = True
            print("Set stop_acquisition_flag")
        return
    elif (('method' in payload) and (payload['method'].startswith("setValue") )):                 # Get power series values
        match = re.search(r'(\d)(\d)$', payload['method'])
        if match:
            condition = int(match.group(1))
            transition = int(match.group(2))
            # print(f"Condition: {condition}, Transition: {transition}")
            condition_transition[condition][transition] = payload['params']
            result = bool_array_to_int(condition_transition)
            # print(result)
            send_condition_transition_matrix(ser)
        else:
            print("No match found")
            print(payload)
            return
    elif (('method' in payload) and (payload['method'].startswith("getValue"))):                             # Respond with the saved value
        match = re.search(r'(\d)(\d)$', payload['method'])
        if match:
            condition = int(match.group(1))
            transition = int(match.group(2))
            # print(f"Condition: {condition}, Transition: {transition}")
            result = condition_transition[condition][transition]
            # print(result)
            client.publish(msg.topic.replace("request", "response"), json.dumps(result), 1)
    else:
        print("Unknown message type")
        print(payload)
        return
    
def send_power_value(ser, h):
    global build_power_h
    print(f'bp{h}:{build_power_h[h]:.1f}\n'.encode())
    ser.write(f'bp{h}:{build_power_h[h]:.1f}\n'.encode())
    
def send_condition_transition_matrix(ser):
    global condition_transition
    result = bool_array_to_int(condition_transition)
    print(f'ct:{result}\n'.encode())
    ser.write(f'ct:{result}\n'.encode())
    
def bool_array_to_int(bool_array):
    # Flatten the array
    flat_list = [item for sublist in bool_array for item in sublist]

    # Convert the list of booleans to a list of integers
    int_list = [int(b) for b in flat_list]

    # Convert the list of integers to a single integer
    result = 0
    for i in int_list:
        result = (result << 1) | i

    return result