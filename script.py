import serial

import paho.mqtt.client as paho
import time
import random as rnd 
import re

import os
import time
import sys
import paho.mqtt.client as mqtt
import json

import utils

# Replace the port and baudrate with the appropriate values for your device
ser = serial.Serial('COM12', 115200)

THINGSBOARD_HOST = 'localhost'
ACCESS_TOKEN = 'umu4Wv60nJOhihfUMiOU'

# State of data acquisition
data_acquisition = False         # True when data acquisition is on
start_acquisition_flag = False   # raised when data acquisition is requested to start (from dashboard)
stop_acquisition_flag = False    # raised when data acquisition is requested to stop (from dashboard)

# Memory for data acquisition
data_acquisition_memory = {'grid_energy':0, 'build_energy':0, 'pv_energy':0, 'wind_energy':0}

# Data capture and upload interval in seconds. Less interval will eventually hang the DHT22.
INTERVAL=0.1

sensor_data = {}

next_reading = time.time() 


userdata = {
    'ser': ser,
    'flags':{
        'data_acquisition': data_acquisition,
        'start_acquisition_flag': start_acquisition_flag,
        'stop_acquisition_flag': stop_acquisition_flag
    }
}
client = mqtt.Client(userdata=userdata)
client.on_message = utils.on_message


# Set access token
client.username_pw_set(ACCESS_TOKEN)

# Connect to ThingsBoard using default MQTT port and 60 seconds keepalive interval
client.connect(THINGSBOARD_HOST, 1883, 60)

client.subscribe('v1/devices/me/rpc/request/+')     # subscribe to RPC requests (start/stop cycle)
client.subscribe('v1/devices/me/attributes')        # subscribe to power reconfiguration update

client.loop_start()

# Retrieve the initial power values
keys = ["build_power_h" + str(i) for i in range(12)]
sharedKeys = ','.join(keys)
client.publish('v1/devices/me/attributes/request/1', json.dumps({"sharedKeys": sharedKeys}), 1)

try:
    while True:
        
        # Read a line of data from the serial port
        data = ser.readline().decode().strip()
        print(data)
        # Convert the values to floats
        try :
            data_floats = [float(value) for value in data.split(':')]
            sensor_data['grid_power'] = data_floats[0]
            sensor_data['grid_voltage'] = data_floats[1]
            sensor_data['grid_current'] = data_floats[2]
            sensor_data['grid_energy'] = data_floats[3] - data_acquisition_memory['grid_energy']
            sensor_data['build_power'] = data_floats[4]
            sensor_data['build_voltage'] = data_floats[5]
            sensor_data['build_current'] = data_floats[6]
            sensor_data['build_energy'] = data_floats[7] - data_acquisition_memory['build_energy']
            sensor_data['pv_power'] = data_floats[8]
            sensor_data['pv_voltage'] = data_floats[9]
            sensor_data['pv_current'] = data_floats[10]
            sensor_data['pv_energy'] = data_floats[11] - data_acquisition_memory['pv_energy']
            sensor_data['wind_power'] = data_floats[12]
            sensor_data['wind_voltage'] = data_floats[13]
            # sensor_data['wind_current'] = data_floats[14]
            # sensor_data['wind_energy'] = data_floats[15] - data_acquisition_memory['wind_energy']
        except:
            # print("Error during the serial port reading")
            continue
        
        if userdata['flags']['stop_acquisition_flag'] and (not userdata['flags']['data_acquisition']):  # data acquisition is already stopped
            userdata['flags']['stop_acquisition_flag'] = False
        
        if userdata['flags']['start_acquisition_flag'] and (not userdata['flags']['data_acquisition']):  # start data acquisition and reset values
            userdata['flags']['data_acquisition'] = True
            userdata['flags']['start_acquisition_flag'] = False
            data_acquisition_memory = { 'grid_energy':data_floats[3],
                                        'build_energy':data_floats[7],
                                        'pv_energy':data_floats[11],
                                        'wind_energy':data_floats[15]}
            # Send the 's' command to the serial port to start power
            ser.write(b's\n')
            print("Data acquisition started")
            
        if userdata['flags']['stop_acquisition_flag'] and userdata['flags']['data_acquisition']:  # stop data acquisition 
            userdata['flags']['data_acquisition'] = False
            userdata['flags']['stop_acquisition_flag'] = False
            # Send the 'i' command to the serial port to stop power
            ser.write(b'i\n')
            print("Data acquisition stopped")
    
        # Sending data to ThingsBoard
        if userdata['flags']['data_acquisition']:
            client.publish('v1/devices/me/telemetry', json.dumps(sensor_data), 1)
            

        next_reading += INTERVAL
        sleep_time = next_reading-time.time()
        if sleep_time > 0:
            time.sleep(sleep_time)
except KeyboardInterrupt:
    pass

client.loop_stop()
client.disconnect()
